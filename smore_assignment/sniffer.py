# sniffer skeleton

#XML, requests
import xml.etree.ElementTree as ET 
import subprocess
import re
#

import os

class Sniffer:
    _USER_DB = "./user.dat"
    _user_db = [] #store registered IMSI of subscribed UEs.

    _XML_HEADER = "<?xml version=\"1.0\"?>\n\
                <?xml-stylesheet type=\"text/xsl\" href=\"pdml2html.xsl\"?>\n\
                <pdml version=\"0\" creator=\"wireshark/1.8.5\" time=\"Mon Jan 20 15:50:53 2014\" \
                capture_file=\"Mme.net_d.Ue_attach_detach\">"
    _XML_END = "</pdml>"

    def _dump_registered_user(self):
        print "Registered users's M-TMSI"
        print self._user_db

    def __init__(self, controller):
        print "Create sniffer\n"
        self.controller = controller
        self._build_database()
        self._dump_registered_user()

	# given function
    def start_sniffing (self, interfaces):
        print "Sniffer starts snooping interface %s \n" % interfaces 
        tshark_out = subprocess.Popen(["sudo","tshark", "-i", interfaces, "-f", "sctp", "-T", "pdml"], stdout=subprocess.PIPE)
        packet_xml = ""
        file_xml = self._XML_HEADER
        for line in iter(tshark_out.stdout.readline, ""):
            packet_xml += line
            if re.match(r'</packet>',line):
                packet_xml += self._XML_END
                self.parse_packet(packet_xml)
                packet_xml = self._XML_HEADER

    def _build_database(self):
        if os.path.isfile(self._USER_DB):
            user_db = open(self._USER_DB)

        line = user_db.readline()
        while line:
            if line.split()[0] != "":
                self._user_db.append(line.split()[0])
                line = user_db.readline()
            else:
                print "NO DATABASE AVAILABLE!"

         
	# partiall given function
    def parse_packet(self, packet):
        print "Sniffer parse_packet funtion\n\n"
        print packet

	# These functions will be implemented
    def dump_ue_attach_event_info(self): 
        pass

    def dump_ue_detach_event_info(self): 
        pass

    def trigger_smore_controller(self):
		dump_ue_attach_event_info(self) 
		dump_ue_detach_event_info(self) 
		self.controller.get_information()

# skeleton for SMORE controller

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet

from sniffer import *
import thread
import time
import re
from ryu import cfg
import subprocess

import logging
LOG = logging.getLogger(__name__)

'''
		You use these values for flow rules. 
        "initial_setup()" function automatically obtains these values 
		after a controller is connected to SMORE SDN.
        self.enb_port_num 
        self.sgw_port_num 
        self.cloud_port_num 
        self.gtp_eval_port_num 
        self.gtp_decap_port_num 
        self.gtp_encap_port_num 
        self.enb_net_d_mac 
        self.sgw_net_d_mme_mac 
        self.cloud_offload_mac 
'''

class SMORE_controller(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
   
    def __init__(self, *args, **kwargs):
        super(SMORE_controller, self).__init__(args, kwargs)
        self.enb_port_num = None  # smore sdn eth port connected to enb
        self.sgw_port_num = None  # smore sdn eth port connected to sgw
        self.cloud_port_num = None # smore sdn eth port connected to cloud
        self.gtp_eval_port_num = None # smore sdn logical port for GTP evaluation
        self.gtp_decap_port_num = None # smore sdn logical port for GTP decap
        self.gtp_encap_port_num = None # smore sdn logical port for GTP encap

        self.enb_net_d_mac = None   # enodeb node mac address
        self.sgw_net_d_mme_mac = None # sgw node mac address
        self.cloud_offload_mac = None # cloud node mac address
 
        self._LISTEN_INF = None # ethernet interface which is monitored

    # get ports info in SMORE node	
    # You can also get these info with OpenFlow API
    def get_smore_sdn_ports_info(self):
        #Get logical interface (eg, net-d) to MAC mapping
        physical_logical_inf_map = subprocess.check_output(["./get_interface_map.pl"], shell=True)
        smore_net_d_mac   = ":".join(re.findall(r'.{1,2}',re.search(r'net-d -> (.*) -> (.*) -> (.*)',physical_logical_inf_map).group(3)))
        smore_net_d_mme_mac  = ":".join(re.findall(r'.{1,2}',re.search(r'net-d-mme -> (.*) -> (.*) -> (.*)',physical_logical_inf_map).group(3)))
        smore_cloud_mac  = ":".join(re.findall(r'.{1,2}',re.search(r'offload -> (.*) -> (.*) -> (.*)',physical_logical_inf_map).group(3)))

        # Retrieve port number for the logical interfaces using MAC address. This Python script has to be run as a root user
        port_desc = subprocess.check_output(["sudo ./ovs_port_desc.sh"], shell=True)
        (net_d_port, self._LISTEN_INF) = re.search(r'(.*)\((.*)\): addr:%s'% smore_net_d_mac, port_desc,re.IGNORECASE).groups()
        net_d_mme_port = re.search(r'(.*)\((.*): addr:%s'% smore_net_d_mme_mac, port_desc, re.IGNORECASE).group(1)
        cloud_port  = re.search(r'(.*)\((.*): addr:%s'% smore_cloud_mac, port_desc, re.IGNORECASE).group(1)
        gtp_eval_port  =  re.search(r'(.*)\(gtp1\): addr:(.*)',port_desc,re.IGNORECASE).group(1)
        gtp_decap_port =  re.search(r'(.*)\(gtp2\): addr:(.*)',port_desc,re.IGNORECASE).group(1)
        gtp_encap_port =  re.search(r'(.*)\(gtp3\): addr:(.*)',port_desc,re.IGNORECASE).group(1)

        self.enb_port_num = int(net_d_port)
        self.sgw_port_num = int(net_d_mme_port)
        self.cloud_port_num = int(cloud_port)
        self.gtp_eval_port_num = int(gtp_eval_port)
        self.gtp_decap_port_num = int(gtp_decap_port)
        self.gtp_encap_port_num = int(gtp_encap_port)


    # Get MAC addresses of cloud, enb's net-d, sgw's net-d-mme interfaces
    def get_neighbor_nodes_macs(self):
        self.logger.debug("\nGetting MACs info for cloud, enb, and sgw\n")
        domain_name = subprocess.check_output(["hostname | sed s/`hostname -s`.//"], shell=True).rstrip()
        SGW_NODE = "sgw.%s" % domain_name
        ENB_NODE = "enb1.%s" % domain_name
        self._CLOUD_NODE = "cloud.%s" % domain_name

        ssh_p = subprocess.Popen(["ssh", ENB_NODE, "ifconfig | grep -B1 192.168.4.90"], stdout=subprocess.PIPE)
        self.enb_net_d_mac = re.search(r'HWaddr (.*)',ssh_p.communicate()[0]).group(1).split()[0]

        ssh_p = subprocess.Popen(["ssh", SGW_NODE, "ifconfig | grep -B1 192.168.4.20"], stdout=subprocess.PIPE)
        self.sgw_net_d_mme_mac = re.search(r'HWaddr (.*)',ssh_p.communicate()[0]).group(1).split()[0]

        ssh_p = subprocess.Popen(["ssh", self._CLOUD_NODE, "ifconfig | grep -B1 192.168.10.11"], stdout=subprocess.PIPE)
        self.cloud_offload_mac = re.search(r'HWaddr (.*)',ssh_p.communicate()[0]).group(1).split()[0]

    # Set default route for cloud node (after _get_neighbor_nodes_macs())
    def set_default_route_in_cloud_node(self):
        self.logger.debug("\nSet default route in routing table  in cloud node\n")
        ssh_p = subprocess.Popen(["ssh", self._CLOUD_NODE, "/usr/local/etc/emulab/findif -i 192.168.10.11"], stdout=subprocess.PIPE)
        cloud_offload_dev = ssh_p.communicate()[0]
        ssh_p = subprocess.Popen(["ssh", self._CLOUD_NODE, "sudo ip route add 192.168.3.0/24 dev %s" % cloud_offload_dev], stdout=subprocess.PIPE)
        ssh_p.communicate()

    def delete_flow_mod(self, datapath, match, out_port):
		ofproto = datapath.ofproto
		parser = datapath.ofproto_parser

		mod = parser.OFPFlowMod(datapath=datapath, command=ofproto.OFPFC_DELETE, out_port=out_port, out_group=ofproto.OFPG_ANY,match=match)
		datapath.send_msg(mod)

    def delete_all_flow(self, datapath):
        self.logger.debug("\nDelete all flows in SMORE SDN\n")
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch()
        self.delete_flow_mod(datapath, match, ofproto.OFPP_ANY)

    def _add_flow(self, datapath, priority, match, actions):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
     
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority, match=match, instructions=inst)
      
        datapath.send_msg(mod)

    '''
    ovs-ofctl add-flow br0 in_port=$eNB,priority=1,actions=output:$sgw
    ovs-ofctl add-flow br0 in_port=$sgw,priority=1,actions=output:$eNB
    '''
    def push_initial_flow_for_l2_switch(self, datapath):
        self.logger.debug("\nInsert initial flows for L2 switchi into SMORE SDN\n")
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        
        match = parser.OFPMatch(in_port=self.enb_port_num)
        actions = []
        actions.append(parser.OFPActionOutput(self.sgw_port_num))
        self._add_flow(datapath,1,match,actions)

        match = parser.OFPMatch(in_port=self.sgw_port_num)
        actions = []
        actions.append(parser.OFPActionOutput(self.enb_port_num))
        self._add_flow(datapath,1,match,actions)


    def initial_setup(self, datapath): 
        self.get_smore_sdn_ports_info()
        self.get_neighbor_nodes_macs() 
        self.set_default_route_in_cloud_node() 
        self.delete_all_flow(datapath)
        self.push_initial_flow_for_l2_switch(datapath)
        
        try:
            thread.start_new_thread( self.start_sniffer, (self._LISTEN_INF,) )
        except:
            print "Error: unable to start sniffer"

    def start_sniffer(self,listen_inf):
        sniffer = Sniffer(self)
        sniffer.start_sniffing(listen_inf)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        self.logger.debug("\nSMORE SDN (dpid=%d) is connected to a controller\n", ev.msg.datapath.id)
        self.initial_setup(ev.msg.datapath)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        self.logger.debug("\nPacket In\n")
    
    def get_information(self):
        self.logger.debug("\nget information from a sniffer\n")
        self.push_flow_for_uplink() 
        self.push_flow_for_downlink()

    def push_flow_for_uplink(self):
        pass

    def push_flow_for_downlink(self):
        pass



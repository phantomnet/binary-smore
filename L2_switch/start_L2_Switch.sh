#!/bin/bash

ovs-vsctl del-br br0

get_interface="perl ./get_interface_map.pl"
net_d=$($get_interface | grep -w net-d[[:blank:]] | awk '{print $3}')
net_d_mme=$($get_interface | grep -w net-d-mme | awk '{print $3}')
offload=$($get_interface | grep offload | awk '{print $3}')




ovs-vsctl add-br br0 
ovs-vsctl set-fail-mode br0 secure
ifconfig $net_d 0.0.0.0
ifconfig $offload 0.0.0.0
ifconfig $net_d_mme 0.0.0.0

ovs-vsctl add-port br0 $net_d
ovs-vsctl add-port br0 $offload
ovs-vsctl add-port br0 $net_d_mme
ovs-vsctl set bridge br0 protocols=OpenFlow10,OpenFlow12,OpenFlow13

net_d_port=$(ovs-ofctl dump-ports-desc br0 | grep $net_d | awk -F"(" '{print $1}')
offload_port=$(ovs-ofctl dump-ports-desc br0 | grep $offload | awk -F"(" '{print $1}')
net_d_mme_port=$(ovs-ofctl dump-ports-desc br0 | grep $net_d_mme | awk -F"(" '{print $1}')


#
#Delete old flows on the bridge's interfaces.
#
echo -e "LAN\t\tIFACE\tPORT-NUMBER "
echo -e "net_d\t\t$net_d\t$net_d_port"
echo -e "offload\t\t$offload\t$offload_port"
echo -e "net_d_mme\t$net_d_mme\t$net_d_mme_port"

ovs-ofctl del-flows br0 in_port="$net_d_port"
ovs-ofctl del-flows br0 in_port="$net_d_mme_port"
ovs-ofctl del-flows br0 in_port="$offload_port"

#
#Simple rules for turning the ovs node into a bridge:
#	1. In comming packets from enb node are forwarded to sgw-mme-sgsn node.
#	2. In comming packets from sgw-mme-sgsn node are forwarded to enb node.
#
ovs-ofctl add-flow br0 in_port="$net_d_port",priority=2,actions=output:"${net_d_mme_port:1}"
ovs-ofctl add-flow br0 in_port="$net_d_mme_port",priority=2,actions=output:"${net_d_port:1}"

if [ $? == 0 ]; then
	echo -e "\nSuccessfully installing bridge's forwarding rules:"
	ovs-ofctl dump-ports-desc br0
	ovs-ofctl dump-flows br0
	exit 0
else
	echo -e "\nFailed installing bridge's forwarding rules"
	exit 1
fi

